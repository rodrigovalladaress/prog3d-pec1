﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Control de la cámara durante el replay de una partida.
/// Según las secciones por las que ha pasado el coche, se va actualizando la posición y la
/// orientación de la cámara. En el caso de que el coche no pase por una sección definida en el
/// circuito, se considera que ha pasado por la sección "NO_SECTION". En este caso, se delega el
/// control de la cámara a GhostControl.
/// </summary>
public class ReplayCameraControl : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private Transform _ghostCarRenderRoot;
    [SerializeField]
    private GhostControl _ghostControl;
    #endregion

    #region Private variables
    private SectionManager _sectionMngr;
    private ReplayCameraData _replayCameraData;
    private int _i;                                         // Iterador de _replayCameraData.
    private float _accumulatedTime;                         // Tiempo transcurrido desde el inicio 
                                                            // de la última sección.
    private ReplayCameraDataElement _currentDataElement;
    private Section _currentSection;
    // Flags
    private bool _updateCamera;
    #endregion

    #region Properties
    public ReplayCameraData CameraData {
        get {
            return _replayCameraData;
        }
        set {
            _replayCameraData = value;
        }
    }

    public bool UpdateCamera {
        get {
            return _updateCamera;
        }
        set {
            _updateCamera = value;
        }
    }

    public SectionManager SectionManager {
        get {
            return _sectionMngr;
        }
        set {
            _sectionMngr = value;
        }
    }
    #endregion

    #region Life cycle
    void Awake () {
        _i = 0;
        _accumulatedTime = 0f;
    }
	
	void FixedUpdate () {
		if(_updateCamera && _i < _replayCameraData.Count) {
            _accumulatedTime += Time.fixedDeltaTime;
            // Primera iteración.
            if (_currentDataElement == null) {
                UpdateCurrentDataElement();
                _accumulatedTime = 0f;
            }
            // Se avanza al siguiente elemento de _replayCameraData.
            if(_accumulatedTime >= _currentDataElement.Duration) {
                _i++;
                UpdateCurrentDataElement();
                _accumulatedTime = 0f;
                // En la sección "NO_SECTION", delegar el movimiento de la cámara a GhostControl.
                if(_currentSection == null) {
                    _ghostControl.UpdateCamera = true;
                } else {
                    _ghostControl.UpdateCamera = false;
                }
            }
            if(_currentSection != null) {
                // Inicialización de la sección.
                if (_accumulatedTime == 0f) {
                    transform.position = _currentSection.CameraSpawnPoint.position;
                    transform.rotation = _currentSection.CameraSpawnPoint.rotation;
                }
                transform.LookAt(_ghostCarRenderRoot.transform);
            }
        }
	}
    #endregion

    #region Methods
    private void UpdateCurrentDataElement() {
        int sectionNumber;
        _currentDataElement = _replayCameraData[_i];
        // Si no es la sección "NO_SECTION".
        if(_currentDataElement != null) {
            sectionNumber = _currentDataElement.Section;
            if (sectionNumber != SectionManager.NO_SECTION) {
                _currentSection = _sectionMngr.Sections[sectionNumber];
            } else {
                _currentSection = null;
            }
        }
    }
    #endregion

}
