﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Gestiona ReplayCameraCreator y ReplayCameraControl.
/// </summary>
public class ReplayCameraManager : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private SectionManager _sectionManager;
    [SerializeField]
    private ReplayCameraControl _replayCameraControl;
    #endregion

    #region Private variables
    private ReplayCameraCreator _replayCameraCrtr;
    #endregion

    #region Properties
    public bool TrackData {
        get {
            return _replayCameraCrtr.TrackData;
        }
        set {
            _replayCameraCrtr.TrackData = value;
        }
    }

    public ReplayCameraCreator ReplayCameraCreator {
        get {
            return _replayCameraCrtr;
        }
    }
    #endregion

    #region Life cycle
    private void Awake() {
        _replayCameraCrtr = GetComponent<ReplayCameraCreator>();
        _replayCameraControl.SectionManager = _sectionManager;
    }
    #endregion

    #region Methods
    public void ActivateCameraControl() {
        _replayCameraCrtr.TrackData = false;
        _replayCameraCrtr.EndLastSection();
        _replayCameraCrtr.RemoveEmptyDataElements();
        _replayCameraControl.CameraData = _replayCameraCrtr.CameraData;
        _replayCameraControl.UpdateCamera = true;
    }

    public void StopTrackingData() {
        TrackData = false;
        _replayCameraCrtr.EndLastSection();
        _replayCameraCrtr.RemoveEmptyDataElements();
    }

    #endregion

}
