﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Datos de la cámara durante el replay de la partida. Estos van guardándose en una lista y cada
/// elemento tiene el marco temporal en el que aplica el dato almacenado.
/// </summary>
public class ReplayCameraData  {

    #region Private variables
    private List<ReplayCameraDataElement> _replayCameraData;
    #endregion

    #region Properties
    public ReplayCameraDataElement Last {
        get {
            ReplayCameraDataElement r = null;
            if(Count > 0) {
                r = _replayCameraData[Count - 1];
            }
            return r;
        }    
    }

    public int Count {
        get {
            return _replayCameraData.Count;
        }
    }

    public ReplayCameraDataElement this[int i] {
        get {
            ReplayCameraDataElement r = null;
            if(i < Count) {
                r = _replayCameraData[i];
            }
            return r;
        }
    }
    #endregion

    #region Constructors
    public ReplayCameraData() {
        _replayCameraData = new List<ReplayCameraDataElement>();
    }
    #endregion

    #region Methods
    public void Add(ReplayCameraDataElement cameraData) {
        _replayCameraData.Add(cameraData);
    }

    public void RemoveLast() {
        _replayCameraData.Remove(Last);
    }
    #endregion
}
