﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Representa una sección del circuito que tiene una misma cámara en el replay de la partida.
/// Recoge todos los triggers de SectionTrigger y los procesa. Informa a SectionManager del estado
/// de las colisiones del coche en función de la sección.
/// Los triggers se procesan en Update.
/// </summary>
public class Section : MonoBehaviour {
    #region Initialize in inspector
    [SerializeField]
    private Transform _cameraSpawnPoint;            // Punto inicial de la cámara.
    /*[SerializeField]
    private float _cameraMaxDistance;               // Máxima distancia que puede separarse la
                                                    // cámara del punto inicial.
    [SerializeField]
    private float _minDistanceFromCar;*/
    [SerializeField]
    private bool _debug;
    #endregion

    #region Private variables
    private SectionManager _sectionMngr;            // Se inicializa en SectionManager.
    private int _number;                            // Se inicializa en SectionManager.
    private string _carTag;
    // Flags
    private bool _sectionEnter;
    private bool _sectionExit;
    private List<int> _childCollisionsEnterNumber;  // Almacena los hijos que han generado un 
                                                    // OnCollisionEnter. 
    private List<int> _childCollisionsStayNumber;   // Ídem con onCollisionStay.
    private List<int> _childCollisionsExitNumber;   // Ídem con onCollisionExit.
    #endregion

    #region Properties
    public string CarTag {
        get {
            return _sectionMngr.CarTag;
        }
    }

    public bool Debug {
        get {
            return _debug;
        }
    }

    public int Number {
        get {
            return _number;
        }
        set {
            _number = value;
        }
    }

    public SectionManager SectionManager {
        get {
            return _sectionMngr;
        }
        set {
            _sectionMngr = value;
        }
    }

    public Transform CameraSpawnPoint {
        get {
            return _cameraSpawnPoint;
        }
    }
    #endregion

    #region Life cycle
    private void Awake() {
        SectionTrigger[] childs = GetComponentsInChildren<SectionTrigger>();
        _childCollisionsEnterNumber = new List<int>();
        _childCollisionsStayNumber = new List<int>();
        _childCollisionsExitNumber = new List<int>();
        InitializeFlags();
        for (int i = 0; i < childs.Length; i++) {
            childs[i].Number = i;
        }
    }

    void Update () {
		if(_sectionEnter) {
            _sectionMngr.OnSectionEnter(_number);
            InitializeFlags();
        } else if(_sectionExit) {
            _sectionMngr.OnSectionExit(_number);
            InitializeFlags();
        }
	}
    #endregion

    #region Methods
    private void InitializeFlags() {
        _sectionEnter = false;
        _sectionExit = false;
        _childCollisionsEnterNumber.Clear();
        _childCollisionsStayNumber.Clear();
        _childCollisionsExitNumber.Clear();
    }

    public void OnChildTriggerEnter(int childNumber) {
        // Se comprueba que este hijo no haya generado un OnTriggerEnter ni que exista un 
        // OnTriggerStay de un hijo distinto, para evitar generar nuevas entradas en secciones
        // dentro de una misma sección (ocurre en las que tengan más de un collider).
        if(!IsIn(childNumber, _childCollisionsEnterNumber) 
            && !IsAnotherIn(childNumber, _childCollisionsStayNumber)) {
            _sectionEnter = true;
            _childCollisionsEnterNumber.Add(childNumber);
        }
    }

    public void OnChildTriggerExit(int childNumber) {
        if (!IsIn(childNumber, _childCollisionsStayNumber) 
            && !IsAnotherIn(childNumber, _childCollisionsStayNumber)) {
            _sectionExit = true;
            _childCollisionsStayNumber.Add(childNumber);
        }
    }

    public void OnChildTriggerStay(int childNumber) {
        if (!IsIn(childNumber, _childCollisionsExitNumber)) {
            _childCollisionsExitNumber.Add(childNumber);
        }
    }

    // ¿El hijo childNumber está en la lista list?
    private bool IsIn(int childNumber, List<int> list) {
        bool r = false;
        int i = 0;
        while(i < list.Count && list[i] != childNumber) {
            i++;
        }
        if(i != list.Count) {
            r = true;
        }
        return r;
    }

    // ¿Hay un hijo distinto a childNumber en list?
    private bool IsAnotherIn(int childNumber, List<int> list) {
        bool r = false;
        int i = 0;
        while (i < list.Count && list[i] == childNumber) {
            i++;
        }
        if (i != list.Count) {
            r = true;
        }
        return r;
    }
    #endregion
}
