﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Se encarga de construir la información de la repetición/fantasma. Captura los datos de posición
/// y rotación del coche del jugador cada _trackDataEachSeconds segundos. Estos datos se guardarán
/// en un objeto GhostData, que luego será usado por GhostControl para simular el movimiento del
/// fantasma.
/// </summary>
public class GhostCreator : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private float _trackDataEachSeconds = 0.2f;             // Segundos que transcurren entre cada
                                                            // captura de datos.
    [SerializeField]
    private Transform[] _gameObjectsToTrackGhostData;       // Partes del coche de las que se van a
                                                            // capturar los datos de posición y
                                                            // rotación.

    [SerializeField]
    private Transform _camera;
    [SerializeField]
    private bool _showWaypoints = false;                    // Si está a true, durante la captura
                                                            // de datos, se instanciarán en la
                                                            // escena unos waypoints que indican la
                                                            // posición que ha seguido el coche
                                                            // (para debugear).
    [SerializeField]
    private GameObject _waypoint;                           // Prefab del waypoint.
    [SerializeField]
    private Transform _waypointsRoot;                      // Padre de los waypoints.
    #endregion

    #region Private variables
    private GhostData _ghostData;
    private bool _trackGhostData;                           // Flag que indica si hay que capturar 
                                                            // los datos del fantasma. Se inicia-
                                                            // liza en GhostManager
    private bool _trackingGhostData;                        // Flag que indica si se está capturan-
                                                            // do datos de un fantasma en este
                                                            // momento.
    private int _iterator;                                  // Iterador de 
                                                            // _gameObjectsToTrackGhostData.
    private bool _waitForNextUpdate;                        // Flag que indica si hay que esperar 
                                                            // para capturar el siguiente dato.
    private float _accumulatedDeltaTime;                    // Tiempo acumulado en los frames en
                                                            // los que se espera a la siguiente
                                                            // captura de datos.
    #endregion

    #region Properties
    public bool TrackGhostData {
        get {
            return _trackGhostData;
        }
        set {
            _trackGhostData = value;
        }
    }

    public GhostData GhostData {
        get {
            return _ghostData;
        }
    }
    #endregion

    #region Lifecycle
    private void Awake() {
        _trackGhostData = false;
        _trackingGhostData = false;
        
        _iterator = 0;
        _waitForNextUpdate = false;
        _accumulatedDeltaTime = 0f;
    }

    private void Start() {
        _ghostData = new GhostData(GetComponent<GameManager>().LapManager.NumberOfLaps);
    }

    private void FixedUpdate() {
        // La primera vez que _trackGhostData se pone a true, se inicializa _ghostData.
        if (_trackGhostData && !_trackingGhostData) {
            for (int i = 0; i < _gameObjectsToTrackGhostData.Length; i++) {
                _ghostData.AddObject(_gameObjectsToTrackGhostData[i].name);
            }
            _trackingGhostData = true;
        }
        if(_trackGhostData && _trackingGhostData) {
            if (!_waitForNextUpdate) {
                // En este frame se van a capturar los datos del coche.
                AddGhostData();
            } else {
                // En este frame hay que esperar a la siguiente captura de datos, por lo que se
                // actualiza _accumulatedDeltaTime con la duración del frame.
                _accumulatedDeltaTime += Time.fixedDeltaTime;
            }
        }
        if(!_trackGhostData && _trackingGhostData) {
            _trackingGhostData = false;
        }
    }
    #endregion

    #region Methods
    private void AddGhostData() {
        for (int i = 0; i < _gameObjectsToTrackGhostData.Length; i++) {
            Transform o = _gameObjectsToTrackGhostData[i];
            Vector3 position = o.position;
            Quaternion rotation = o.rotation;
            _ghostData.AddGhostData(i, position, rotation);
            if (_showWaypoints) {
                GameObject waypoint;
                if (_waypoint) {
                    waypoint = Instantiate(_waypoint, position, rotation) as GameObject;
                }
                else {
                    waypoint = Instantiate(new GameObject(), position, rotation) as GameObject;
                }
                waypoint.name = o.name + "_" +  _iterator;
                if(_waypointsRoot) {
                    waypoint.transform.parent = _waypointsRoot;
                }
                
            }
        }
        if(_showWaypoints) {
            _iterator++;
        }
        if (_camera) {
            _ghostData.AddCameraData(_camera.position, _camera.rotation);
        }
        _ghostData.AddDeltaTime(_accumulatedDeltaTime);
        _accumulatedDeltaTime = Time.fixedDeltaTime;
        StartCoroutine(WaitForNextUpdate());
    }
    
    // Actualiza el flag _waitForNextUpdate tras _trackDataEachSeconds segundos.
    private IEnumerator WaitForNextUpdate() {
        _waitForNextUpdate = true;
        yield return new WaitForSeconds(_trackDataEachSeconds);
        _waitForNextUpdate = false;
    }

    public void PrintParts() {
        string print = "{";
        foreach (var item in _gameObjectsToTrackGhostData) {
            print += item.name + ", ";
        }
        print += "}";
        Debug.Log(print);
    }
    #endregion
}
