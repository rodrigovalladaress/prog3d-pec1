﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Comportamiento de la línea de meta.
/// </summary>
public class Finish : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private GameManager _gameManager;
    [SerializeField]
    private bool _debug;
    #endregion

    #region Private variables
    private LapManager _lapMngr;
    private string _carTag;
    private bool _checkTrigger;
    #endregion

    #region Lifecycle
    private void Awake() {
        _checkTrigger = true;
    }

    private void Start() {
        _carTag = _gameManager.Car.tag;
        _lapMngr = _gameManager.LapManager;
    }
    #endregion

    #region Triggers
    private void OnTriggerEnter(Collider other) {
        if (_debug) {
            Debug.Log("Finish On Trigger Enter with " + other.tag);
        }
        if (_checkTrigger && other.tag == _carTag) {
            _lapMngr.NewLap();
            _checkTrigger = false;
            if (_debug) {
                Debug.Log("Other collider is car: New lap.");
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        if (_debug) {
            Debug.Log("Finish On Trigger Exit with " + other.tag);
        }
        if (other.tag == _carTag) {
            _checkTrigger = true;
        }
    }
    #endregion
}
