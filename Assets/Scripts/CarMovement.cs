﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovement {

    float _steering;
    float _accel;
    float _footbrake;
    float _handbrake;
    int _framesToRepeat;    // Número de frames que se va a repetir este movimiento.
    float _waitTime;

    public CarMovement(float steering, float accel, float footbrake, float handbrake, float waitTime) {
        /*Debug.Log("new CarMovment: {\r\n  steering: " + steering + ",\r\n  accel:" + accel 
            + ",\r\n  footbrake: " + footbrake + ",\r\n  handbrake: " + handbrake+ "\r\n}");*/
        //Debug.Log("new CarMovement: " + steering + ", " + accel + ", " + footbrake + ", " + handbrake);
        _steering = steering;
        _accel = accel;
        _footbrake = footbrake;
        _handbrake = handbrake;
        _framesToRepeat = 0;
        _waitTime = waitTime;
    }

    public float Steering {
        get {
            return _steering;
        }
    }

    public float Accel {
        get {
            return _accel;
        }
    }

    public float Footbrake {
        get {
            return _footbrake;
        }
    }

    public float Handbrake {
        get {
            return _handbrake;
        }
    }

    public int FramesToRepeat {
        get {
            return _framesToRepeat;
        }
    }

    public float WaitTime {
        get {
            return _waitTime;
        }

        set {
            _waitTime = value;
        }
    }

    public bool HasSameValuesAs(float steering, float accel, float footbrake, float handbrake) {
        return _steering == steering && _accel == accel && _footbrake == footbrake 
            && _handbrake == handbrake;
    }

    public void IncreaseFramesToRepeat() {
        _framesToRepeat++;
        //Debug.Log("_framesToRepeat = " + _framesToRepeat);
    }

}
