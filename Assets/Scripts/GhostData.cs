﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Datos del fantasma de un coche.
/// </summary>
public class GhostData {
    #region Constants
    private const int DEFAULT_NUMBER_OF_LAPS = 3;
    private const int GHOST_DATA_LIST_CAPACITY = 250;       // Capacidad inicial de _ghostData
                                                            // (por cada vuelta).
    #endregion

    #region Private variables
    List<string> _names;                                    // Nombres de los objetos.
    List<List<GhostDataElement>> _ghostData;                // Lista de los datos del fantasma en
                                                            // un momento determinado. El primer
                                                            // nivel es el tiempo y, el segundo,
                                                            // una parte del coche.
    List<GhostDataElement> _cameraData;                     // Datos de la cámara.
    List<float> _deltaTimes;                                // Tiempo transcurrido entre cada 
                                                            // dato del fantasma.
    Dictionary<string, int> _listPositions;
    #endregion

    #region Properties
    public int TimeCount {
        get {
            int r = 0;
            if (_ghostData != null) {
                r = _ghostData[0].Count;
            }
            return r;
        }
    }
    #endregion

    #region Constructors
    public GhostData() {
        Initialize(DEFAULT_NUMBER_OF_LAPS);
    }

    public GhostData(int numberOfLaps) {
        Initialize(numberOfLaps);
    }

    private void Initialize(int numberOfLaps) {
        _names = new List<string>();
        _ghostData = new List<List<GhostDataElement>>(GHOST_DATA_LIST_CAPACITY * numberOfLaps);
        _listPositions = new Dictionary<string, int>();
        _cameraData = new List<GhostDataElement>(GHOST_DATA_LIST_CAPACITY * numberOfLaps);
        _deltaTimes = new List<float>();
    }
    #endregion

    #region Methods
    public void AddObject(string name) {
        int namePos;
        _names.Add(name);
        namePos = _names.Count - 1;
        _listPositions.Add(name, namePos);
        _ghostData.Add(new List<GhostDataElement>());
    }

    public void AddGhostData(string name, Vector3 position, Quaternion rotation) {
        int i = _listPositions[name];
        AddGhostData(i, position, rotation);
        Debug.LogWarning("Dont use AddData(string name, Vector3 position, Quaternion rotation)" +
            "but AddData(int i, Vector3 position, Quaternion rotation) for better performance.");
    }

    public void AddGhostData(int i, Vector3 position, Quaternion rotation) {
        _ghostData[i].Add(new GhostDataElement(position, rotation));
    }

    public void AddCameraData(Vector3 position, Quaternion rotation) {
        _cameraData.Add(new GhostDataElement(position, rotation));
    }

    public void AddDeltaTime(float deltaTime) {
        _deltaTimes.Add(deltaTime);
    }

    public GhostDataElement GetGhostData(string name, int t) {
        GhostDataElement r = null;
        int i;
        if (_listPositions.ContainsKey(name)) {
            i = _listPositions[name];
            r = GetGhostData(i, t);
        }
        Debug.LogWarning("Dont use GetData(string name, int t) but GetData(int i, int t) for "
            + "better performance.");
        return r;
    }

    public GhostDataElement GetGhostData(int i, int t) {
        GhostDataElement r = null;
        if (i < _listPositions.Count && t < TimeCount && t >= 0) {
            r = _ghostData[i][t];
        }
        return r;
    }

    public GhostDataElement GetCameraData(int t) {
        GhostDataElement r = null;
        if (t < TimeCount && t >= 0) {
            r = _cameraData[t];
        }
        return r;
    }

    public float GetDeltaTime(int t) {
        float r = 0f;
        if (t < TimeCount && t >= 0) {
            r = _deltaTimes[t];
        }
        return r;
    }
    #endregion

}
