﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Creación del objeto ReplayCameraData en función de las secciones por las que ha pasado el coche
/// durante el transcurso de la partida.
/// StartSection(), EndSection() y NoCollisions() son llamados desde SectionManager.
/// </summary>
public class ReplayCameraCreator : MonoBehaviour {

    #region Constants
    private const float MIN_SECTION_DURATION = 0.2f;   // Las secciones con una duración menor a la
                                                       // aquí indicada no se tendrán en cuenta.
    #endregion

    #region Initialize in inspector
    [SerializeField]
    private bool _debug;
    #endregion

    #region Private variables
    private float _startTime;                          // Momento en el que empezó la última sección.
    private ReplayCameraData _cameraData;
    private int _sectionToStart;
    private int _sectionToEnd;
    // Flags
    private bool _trackData;
    private bool _startSection;                        
    private bool _endSection;
    private bool _noCollisions;
    #endregion

    #region Properties
    public bool TrackData {
        get {
            return _trackData;
        }
        set {
            _trackData = value;
            if (value) {
                _startTime = Time.time;
            }
        }
    }

    public ReplayCameraData CameraData {
        get {
            return _cameraData;
        }
    }
    #endregion

    #region Life cycle
    private void Awake() {
        _startTime = 0f;
        _trackData = false;
        _startSection = false;
        _endSection = false;
        _noCollisions = false;
        _cameraData = new ReplayCameraData();
    }

    // Update is called once per frame
    void FixedUpdate() {
        if (TrackData) {
            float duration = Time.time - _startTime;
            if (_endSection && _cameraData.Last.Section == _sectionToEnd) {
                _cameraData.Last.Duration = duration;
                _endSection = false;
                if (_debug) {
                    Debug.Log("Ended section " + _sectionToEnd);
                    PrintCameraData();
                }
            }
            if (_startSection) {
                _startTime = Time.time;
                _cameraData.Add(new ReplayCameraDataElement(_sectionToStart));
                _startSection = false;
                if (_debug) {
                    Debug.Log("Started section " + _sectionToStart);
                    PrintCameraData();
                }
            }
            if (_noCollisions) {
                _startTime = Time.time;
                _cameraData.Add(new ReplayCameraDataElement(SectionManager.NO_SECTION));
                _noCollisions = false;
                if (_debug) {
                    Debug.Log("Started section " + SectionManager.NO_SECTION);
                    PrintCameraData();
                }
            }
        }
    }
    #endregion

    #region Methods
    // Eliminar las secciones con una duración menor a MIN_SECTION_DURATION y añadir su duración
    // a la siguiente sección válida.
    public void RemoveEmptyDataElements() {
        ReplayCameraData newCameraData = new ReplayCameraData();
        float accumulatedDuration = 0f;
        if (_cameraData.Count > 0) {
            for (int i = 0; i < _cameraData.Count; i++) {
                ReplayCameraDataElement dataElement = _cameraData[i];
                if (dataElement.Duration >= MIN_SECTION_DURATION) {
                    dataElement.Duration += accumulatedDuration;
                    newCameraData.Add(dataElement);
                    accumulatedDuration = 0f;
                } else {
                    accumulatedDuration += dataElement.Duration;
                    if(_debug) {
                        Debug.Log("Removed {" + dataElement.Section + " -> " 
                            + dataElement.Duration + "}");
                    }
                }
            }
            _cameraData = newCameraData;
        }
        if (_debug) {
            Debug.Log("Removed empty data elements");
            PrintCameraData();
        }
    }

    public void EndLastSection() {
        _cameraData.Last.Duration = Time.time - _startTime;
        if (_debug) {
            Debug.Log("Ended last section");
            PrintCameraData();
        }
    }

    public void StartSection(int section) {
        if (_trackData) {
            if (_cameraData.Last == null || _cameraData.Last.Section != section) {
                _startSection = true;
                _sectionToStart = section;
            }
            // Si la última sección es "NO_SECTION" o tiene duración 0, debe cerrarse, ya que
            // unca habrá un evento de cierre de una sección "NO_SECTION".
            if(_cameraData.Last != null && (_cameraData.Last.Section == SectionManager.NO_SECTION
                || _cameraData.Last.Duration == 0f)) {
                _endSection = true;
                _sectionToEnd = _cameraData.Last.Section;
            }
        }
    }

    public void EndSection(int section) {
        if (_trackData) {
            _endSection = true;
            _sectionToEnd = section;
        }
    }

    public void NoCollisions() {
        if (_cameraData.Count > 0 // Evitar añadir una sección al principio de la partida.
            && _cameraData.Last.Section != SectionManager.NO_SECTION) {
            _noCollisions = true;
        }

    }

    private void PrintCameraData() {
        string r = "{";
        for(int i = 0; i < _cameraData.Count; i++) {
            ReplayCameraDataElement e = _cameraData[i];
            r += e.Section + " -> " + e.Duration + " s" 
                + (i != _cameraData.Count - 1 ? ",\r\n" : "");
        }
        r += "}";
        Debug.Log(r);
    }

    #endregion
}
