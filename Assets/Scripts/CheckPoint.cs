﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Comportamiento de un checkpoint.
/// </summary>
public class CheckPoint : MonoBehaviour {

    [SerializeField]
    private GameManager _gameManager;
    [SerializeField]
    private bool _debug;

    private int _number;                // Se inicializa en LapManager.
    private string _carTag;
    private LapManager _lapManager;

    public int Number {
        get { return _number; }
        set { _number = value; }
    }

    void Start() {
        _carTag = _gameManager.Car.tag;
        _lapManager = _gameManager.LapManager;
    }

    private void OnTriggerEnter(Collider other) {
        if (other.tag == _carTag) {
            _lapManager.OnCheckpointPassed(_number);
            if (_debug) {
                Debug.Log("Checkpoint " + _number + " passed.");
            }
        }
    }
}
