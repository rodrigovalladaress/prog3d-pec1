﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Gestiona el fantasma.
/// </summary>
public class GhostManager : MonoBehaviour {
    #region Initialize in inspector
    [SerializeField]
    private GhostControl _ghostControl;
    #endregion

    #region Private variables
    private GhostCreator _ghostCreator;
    #endregion

    #region Properties
    public bool TrackGhostData {
        get {
            return _ghostCreator.TrackGhostData;
        }
        set {
            _ghostCreator.TrackGhostData = value;
        }
    }
    #endregion

    #region Lifecycle
    private void Awake() {
        _ghostCreator = GetComponent<GhostCreator>();
    }
    #endregion

    #region Methods
    public GhostControl InstantiateGhost() {
        _ghostControl.gameObject.SetActive(true);
        _ghostControl.GhostData = _ghostCreator.GhostData;
        _ghostControl.UpdateGhost = true;
        _ghostCreator.TrackGhostData = false;
        return _ghostControl;
    }
    #endregion

}
