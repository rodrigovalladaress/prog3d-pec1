﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// CarController modificado para cumplir con los requisitos de la PEC 1.
/// </summary>
public class CarControllerPEC1 : UnityStandardAssets.Vehicles.Car.CarController {

    #region InitializeInInspector
    [SerializeField]
    private float _percentSpeedReductionOffRoad = 0.6f;
    #endregion

    #region Properties
    public float TopSpeed {
        get { return m_Topspeed; }
        set { m_Topspeed = value; }
    }

    public float PercentSpeedReductionOffRoad {
        get { return _percentSpeedReductionOffRoad; }
    }
    #endregion

    // Use this for initialization
    new void Start () {
        base.Start();
	}

}
