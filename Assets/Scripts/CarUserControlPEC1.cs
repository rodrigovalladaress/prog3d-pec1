﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;
/// <summary>
/// Modificación de CarUserControl de Standard Assets.
/// </summary>
[RequireComponent(typeof(CarControllerPEC1))]

public class CarUserControlPEC1 : MonoBehaviour {

    private CarControllerPEC1 _carCtrl; // the car controller we want to use
    private GameManager _gameMngr;

    public GameManager GameManager {
        get { return _gameMngr; }
        set { _gameMngr = value; }
    }

    private void Start() {
        _carCtrl = _gameMngr.Car.GetComponent<CarControllerPEC1>();
    }

    private void FixedUpdate() {
        float h = CrossPlatformInputManager.GetAxis("Horizontal");
        float v = CrossPlatformInputManager.GetAxis("Vertical");
        float handbrake = CrossPlatformInputManager.GetAxis("Jump");
        //if (h != 0f || v != 0f || handbrake != 0f) {
        _carCtrl.Move(h, v, v, handbrake);
        //_gameMngr.OnCarMoved(h, v, v, handbrake);
        //}
    }
}
