﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Comprueba los objetos con los que colisiona una rueda del coche. Si esta está en contacto con
/// un GameObject que no sea la carretera, la velocidad se reducirá. Cuando vuelva a estar en
/// contacto con la carretera (GameObject con tag _roadTag), se restaurará a su valor original.
/// </summary>
public class WheelGroundCheck : MonoBehaviour {
    #region Initialize in inspector
    [SerializeField]
    private string _roadTag = "road";
    #endregion

    #region Private variables
    private CarControllerPEC1 _carCtrl;
    private float _startingTopSpeed;        // Velocidad máxima inicial del coche.
    private bool _speedReduced;             // Indica si la velocidad se ha reducido, para evitar
                                            // que esta se reduzca más de una vez por rueda.
    private List<string> _tagCollisions;    // Tags de los objetos que han colisionado con la 
                                            // rueda.
                                            // Se utiliza para comprobar los objetos que han coli-
                                            // sionado en Update.
    #endregion

    #region Life cycle
    private void Awake() {
        _carCtrl = GetComponentInParent<CarControllerPEC1>();
        _startingTopSpeed = _carCtrl.TopSpeed;
        _speedReduced = false;
        _tagCollisions = new List<string>();
    }

    // Reduce o restaura la velocidad el coche dependiendo de los GameObject con los que ha coli-
    // sionado la rueda.
    void Update() {
        if (_tagCollisions.Find(i => i == _roadTag) == null) {
            ReduceSpeed();
        }
        else {
            RestoreSpeed();
        }
    }
    #endregion

    #region Methods
    private void ReduceSpeed() {
        if (!_speedReduced) {
            _carCtrl.TopSpeed -= ((_startingTopSpeed * _carCtrl.PercentSpeedReductionOffRoad) / 4);
            _speedReduced = true;
        }
    }

    private void RestoreSpeed() {
        if (_speedReduced) {
            _carCtrl.TopSpeed += ((_startingTopSpeed * _carCtrl.PercentSpeedReductionOffRoad) / 4);
            _speedReduced = false;
        }
    }
    #endregion

    #region Collisions
    private void OnTriggerEnter(Collider other) {
        if (_tagCollisions.Find(i => i == other.tag) == null) { // Evita añadir tags duplicados
            _tagCollisions.Add(other.tag);
        }
    }

    private void OnTriggerExit(Collider other) {
        _tagCollisions.Remove(other.tag);
    }
    #endregion

}
