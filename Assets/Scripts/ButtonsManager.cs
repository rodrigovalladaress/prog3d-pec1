﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonsManager : MonoBehaviour {

    [SerializeField]
    private GameManager _gameManager;
    [SerializeField]
    private Button _replayButton;
    [SerializeField]
    private Button _ghostButton;

    public void OnReplayButton() {
        _gameManager.Replay();
    }

    public void OnGhostButton() {
        _gameManager.Ghost();
    }

    public void SetButtonsActive(bool active) {
        _replayButton.gameObject.SetActive(active);
        _ghostButton.gameObject.SetActive(active);
    }
}
