﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

public class WaypointsManager : MonoBehaviour {

    [SerializeField]
    private GameManager _gameManager;

    [SerializeField]
    private GameObject _waypointsRoot;

    [SerializeField]
    private GameObject _waypointCar;
    
    private WaypointsCreator _waypointsCreator;
    private WaypointCircuit _waypointCircuit;

    public bool CalculateWaypoints {
        get { return _waypointsCreator.CalculateWaypoints; }
        set {
            _waypointsCreator.CalculateWaypoints = value;
        }
    }

    private void Awake() {
        _waypointsCreator = GetComponent<WaypointsCreator>();
        _waypointsCreator.Car = _gameManager.Car;
        _waypointsCreator.Root = _waypointsRoot.transform;
        _waypointCircuit = _waypointsRoot.GetComponent<WaypointCircuit>();
    }

	// Use this for initialization
	void Start () {
        CalculateWaypoints = true;
	}

    public GameObject InstantiateGhost() {
        WaypointList waypoints = _waypointsCreator.Waypoints;
        GameObject ghost = _waypointCar;
        //_waypointCircuit = _waypointsRoot.AddComponent<WaypointCircuit>();
        _waypointCircuit.waypointList.items = waypoints.ToTransformArray();
        _waypointCircuit.Initialize();
        _waypointCircuit.CachePositionsAndDistances();
        //_waypointCircuit.gameObject.SetActive(true);
        _waypointCar.GetComponent<WaypointProgressTracker>().Circuit = _waypointCircuit;
        _waypointCar.SetActive(true);
        
        Debug.Log("instantiated ghost");
        return ghost;
    }
}
