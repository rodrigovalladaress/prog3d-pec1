﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarMovementList {

	private List<CarMovement> _carMovementList;

    public int Count {
        get {
            return _carMovementList.Count;
        }
    }

    public CarMovement Last {
        get {
            return _carMovementList.Count > 0 ?
                _carMovementList[_carMovementList.Count - 1] : null;
        }
    }

    public CarMovement this[int i] {
        get {
            return _carMovementList[i];
        }
    }

    public CarMovementList() {
        _carMovementList = new List<CarMovement>();
    }

    public void Add(float steering, float accel, float footbrake, float handbrake, float waitTime) {
        _carMovementList.Add(new CarMovement(steering, accel, footbrake, handbrake, waitTime));
    }

}
