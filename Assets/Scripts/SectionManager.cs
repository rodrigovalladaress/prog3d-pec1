﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Recoge todas las colisiones que se han realizado en todas las secciones. Informa de estas 
/// colisiones a ReplayCameraCreator para que esta cree la información de la replay camera.
/// </summary>
public class SectionManager : MonoBehaviour {

    #region Constants
    public const int NO_SECTION = -1;                   // La sección "NO_SECTION" es aquella en la
                                                        // que el coche no está colisionando con 
                                                        // ninguna de las secciones definidas en el
                                                        // circuito.
    #endregion

    #region Initialize in inspector
    [SerializeField]
    private GameManager _gameManager;
    #endregion

    #region Private variables
    private Section[] _sections;                        // Array de las distintas secciones. Estas
                                                        // deben ser hijos de SectionManager.
    private bool[] _collisions;                         // Array que indica si el coche está coli-
                                                        // sionando con la sección i en el momento
                                                        // actual.
    private ReplayCameraCreator _replayCamCrtr;
    private string _carTag;
    // Flags
    private bool _noCollisionsInformed;                 // ¿Se ha informado a ReplayCameraCreator
                                                        // de que el car está en la sección
                                                        // "NO_SECTION"?
    #endregion

    #region Properties
    public string CarTag {
        get {
            return _carTag;
        }
    }

    public int SectionsCollidingCount {
        get {
            int r = 0;
            foreach (bool isColliding in _collisions) {
                if (isColliding) {
                    r++;
                }
            }
            return r;
        }
    }

    public Section[] Sections {
        get {
            return _sections;
        }
    }
    #endregion

    #region Life cycle
    private void Awake() {
        _sections = GetComponentsInChildren<Section>();
        _collisions = new bool[_sections.Length];
        for(int i = 0; i < _sections.Length; i++) {
            _sections[i].Number = i;
            _sections[i].SectionManager = this;
        }
        _noCollisionsInformed = false;
    }

    private void Start() {
        _replayCamCrtr = _gameManager.ReplayCameraManager.ReplayCameraCreator;
        _carTag = _gameManager.Car.tag;
    }

    

    void Update() {
        if (!_noCollisionsInformed && SectionsCollidingCount == 0) {
            _replayCamCrtr.NoCollisions();
            _noCollisionsInformed = true;
        }
    }
    #endregion

    #region Methods
    public void OnSectionEnter(int section) {
        _collisions[section] = true;
        _replayCamCrtr.StartSection(section);
        _noCollisionsInformed = false;
    }

    public void OnSectionExit(int section) {
        _collisions[section] = false;
        _replayCamCrtr.EndSection(section);
        _noCollisionsInformed = false;
    }
    #endregion
}
