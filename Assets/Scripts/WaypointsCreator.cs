﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Registra el movimiento de un coche (_target) para calcular una ruta de waypoints.
/// </summary>
public class WaypointsCreator : MonoBehaviour {
    #region InitializeInInspector    
    [SerializeField]
    private float _milisecsBetweenWaypoints = 500f; // Milisegundos que pasan entre cada crea-
                                                    // ción de un waypoint. 

    [SerializeField]
    private GameObject _wayPointPrefab;             // Prefab de los waypoints (opcional).
    #endregion

    #region Private variables
    private bool _calculateWaypoints;               // Indica si hay que calcular nuevos waypoints.
    private bool _calculatingWaypoints;             // Indica si se están calculando waypoints.
    private Transform _root;                        // GameObject del que colgarán todos los
                                                    // waypoints (se inicializa en 
                                                    // WaypointsManager).
    private GameObject _car;                     // GameObject del coche (se inicializa en
                                                    // WaypointsManager).

    private WaypointList _waypoints;                // Lista de waypoints creados.
    #endregion

    #region Properties
    public bool CalculateWaypoints {
        get { return _calculateWaypoints; }
        set { _calculateWaypoints = value; }
    }
    public WaypointList Waypoints {
        get { return _waypoints; }
    }

    public Transform Root {
        get { return _root; }
        set { _root = value; }
    }

    public GameObject Car {
        get { return _car; }
        set { _car = value; }
    }
    #endregion

    #region Life cycle
    private void Awake() {
        _calculatingWaypoints = false;
        _waypoints = new WaypointList();
    }

    void Update() {
        if (_calculateWaypoints && !_calculatingWaypoints) {
            StartCoroutine(CreateWaypoints());
        }
        else if (!_calculateWaypoints && _calculatingWaypoints) {
            StopCoroutine(CreateWaypoints());
        }
	}
    #endregion

    private IEnumerator CreateWaypoints() {
        _calculatingWaypoints = true;
        while (_calculateWaypoints) {
            Vector3 newPos = _car.transform.position;
            if (_calculateWaypoints && (!_waypoints.Last
                || PECUtility.NearlyEqual(_waypoints.Last.transform.position, newPos))) {
                GameObject waypoint;
                if (_wayPointPrefab) {
                    waypoint = Instantiate(_wayPointPrefab);
                }
                else {
                    waypoint = new GameObject();
                }
                waypoint.transform.position = newPos;
                waypoint.transform.parent = _root;
                _waypoints.Add(waypoint);
            }
            yield return new WaitForSeconds(_milisecsBetweenWaypoints / 1000f);
        }
        _calculatingWaypoints = false;
    }
}