﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Datos de la cámara durante el replay en un momento determinado.
/// </summary>
public class ReplayCameraDataElement {

    #region Private variables
    private int _section;       // Sección en la que está el coche en el momento actual.
    private float _duration;    // Tiempo durante el que el coche está en la sección definida.
    #endregion

    #region Properties
    public int Section {
        get {
            return _section;
        }
    }

    public float Duration {
        get {
            return _duration;
        }
        set {
            _duration = value;
        }
    }
    #endregion

    #region Constructors
    public ReplayCameraDataElement(int section) {
        _section = section;
        _duration = 0f;
    }

    public ReplayCameraDataElement(int section, float duration) {
        _section = section;
        _duration = duration;
    }
    #endregion

}
