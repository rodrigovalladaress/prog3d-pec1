﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Lista de waypoints. Cuando se añade uno nuevo, lo nombra de acuerdo a su posición en la lista.
/// </summary>
public class WaypointList  {

    private const string WAYPOINT_NAME_FORMAT = "0000000000";

    private List<GameObject> _waypoints;

    private void Setup() {
        _waypoints = new List<GameObject>();
    }

    public WaypointList() {
        Setup();
    }

    public GameObject this[int i] {
        get {
            GameObject r = null;
            if (i > 0 && i < Count) {
                r = _waypoints[i];
            }
            return r;
        }
    }

    public Transform[] ToTransformArray() {
        List<Transform> l = new List<Transform>();
        foreach (GameObject waypoint in _waypoints) {
            l.Add(waypoint.transform);
        }
        return l.ToArray();
    }

    public int Count {
        get { return _waypoints.Count; }
    }

    public GameObject Last {
        get { return this[Count - 1]; }
    }

    public void Add(GameObject waypoint) {
        _waypoints.Add(waypoint);
        waypoint.name = (Count - 1).ToString(WAYPOINT_NAME_FORMAT);
    }

}
