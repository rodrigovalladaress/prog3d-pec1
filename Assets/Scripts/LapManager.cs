﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Gestión de las vueltas al circuito.
/// </summary>
public class LapManager : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private int _numberOfLaps = 3;
    [SerializeField]
    private CheckPoint[] _checkpoints;  // El coche debe atravesar todos los checkpoints para
                                        // considerar que la vuelta ha sido válida.
    [SerializeField]
    private bool _debug;
    #endregion

    #region Private variables
    private int _currentLap;
    private bool _checkTime;            // Flag que indica si se debe monitorizar el tiempo de la
                                        // vuelta actual.
    private float[] _startTime;         // Tiempo en el que se empieza a contar el tiempo 
                                        // transcurrido para cada vuelta (Time.time).
    private float[] _lapTime;           // Tiempo transcurrido en cada una de las vueltas.
    private bool[] _checkpointPassed;   // Flags que indican si el coche ha pasado por cada 
                                        // checkpoint.
    private GameManager _gameMngr;
    private GUIManager _guiMngr;
    #endregion

    #region Properties
    public GameManager GameManager {
        get {
            return _gameMngr;
        }
        set {
            _gameMngr = value;
        }
    }

    public int NumberOfLaps {
        get {
            return _numberOfLaps;
        }
    }
    #endregion

    #region Lifecycle
    private void Awake() {
        _guiMngr = GetComponent<GUIManager>();
        _startTime = new float[_numberOfLaps];
        _lapTime = new float[_numberOfLaps];
        _checkpointPassed = new bool[_checkpoints.Length];
        _currentLap = 0;
        _checkTime = true;
        for (int i = 0; i < _checkpoints.Length; i++) {
            _checkpoints[i].Number = i;
        }
        SetCheckpointPassed(false);
    }

	void Start () {
        _startTime[0] = Time.time;
        UpdateLapNumberGUI();
	}
	
	void Update () {
        if (_checkTime) {
            _lapTime[_currentLap] = Time.time - _startTime[_currentLap];
            UpdateLapTimeGUI();
        }
	}
    #endregion

    #region Methods
    private void SetCheckpointPassed(bool value) {
        for (int i = 0; i < _checkpointPassed.Length; i++) {
            _checkpointPassed[i] = value;
        }
    }

    private bool CheckIfAllCheckpointsPassed() {
        bool r = false;
        int i = 0;
        while (i < _checkpointPassed.Length && !_checkpointPassed[i]) {
            i++;
        }
        if (i < _checkpointPassed.Length) {
            r = true;
        }
        return r;
    }

    public void NewLap() {
        if (_currentLap < _numberOfLaps) {
            if (CheckIfAllCheckpointsPassed()) {
                _currentLap++;
                // Si no es la última vuelta, debe actualizarse la GUI, _startTime de la 
                // siguiente vuelta, etc.
                if (_currentLap < _numberOfLaps) {
                    if (_debug) {
                        Debug.Log("All checkpoints passed, new lap (" + _currentLap + ")");
                    }
                    _startTime[_currentLap] = Time.time;
                    UpdateLapNumberGUI();
                    SetCheckpointPassed(false);
                    _gameMngr.OnNewLap(_currentLap, _numberOfLaps);
                }
                // Si es la última vuelta, debe pararse el contador de tiempo.
                else {
                    if (_debug) {
                        Debug.Log("Stop checking time lap");
                    }
                    _checkTime = false;
                    _gameMngr.OnRaceFinished();
                }
            }
            else {
                if (_debug) {
                    Debug.Log("Not all checkpoints have been passed...");
                }
            }
        }
    }

    // Este método es llamado desde la clase Checkpoint.
    public void OnCheckpointPassed(int checkpoint) {
        _checkpointPassed[checkpoint] = true;
    }

    private void UpdateLapNumberGUI() {
        if (_guiMngr) {
            _guiMngr.UpdateLapNumber(_currentLap);
        }
    }

    private void UpdateLapTimeGUI() {
        if(_guiMngr)
        _guiMngr.UpdateLapTime(_currentLap, _lapTime[_currentLap]);
    }
    #endregion
}
