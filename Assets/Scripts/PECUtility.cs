﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Funciones útiles para todas las clases.
/// </summary>
public class PECUtility {

    private const float EPSILON = 0.001f;

    private static float _epsilon = EPSILON;

    public static bool NearlyEqual(Transform a, Transform b) {
        return NearlyEqual(a, b, _epsilon);
    }

    public static bool NearlyEqual(Transform a, Transform b, float epsilon) {
        return NearlyEqual(a.position, b.position, epsilon);
    }

    public static bool NearlyEqual(Vector3 a, Vector3 b) {
        return NearlyEqual(a, b, _epsilon);
    }

    public static bool NearlyEqual(Vector3 a, Vector3 b, float epsilon) {
        return NearlyEqual(a.x, b.x, epsilon)
        && NearlyEqual(a.y, b.y, epsilon)
        && NearlyEqual(a.z, b.z, epsilon);
    }

    public static bool NearlyEqual(float a, float b) {
        return NearlyEqual(a, b, _epsilon);
    }

    public static bool NearlyEqual(float a, float b, float epsilon) {
        return Mathf.Abs(a - b) > epsilon;
    }

}
