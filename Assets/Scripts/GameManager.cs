﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
/// <summary>
/// Gestiona el ciclo de vida de la partida.
/// </summary>
public class GameManager : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private GameObject _car;
    [SerializeField]
    private GameObject _cameraRig;
    #endregion

    #region Private variables
    private UnityStandardAssets.Cameras.AutoCam _autoCam;
    private ReplayCameraControl _replayCameraCtrl;
    private ReplayCameraManager _replayCameraMngr;
    private LapManager _lapMngr;
    private GhostManager _ghostMngr;
    private ButtonsManager _buttonsMngr;
    private CarUserControlPEC1 _carUserCtrl;
    private CarControllerPEC1 _carCtrller;
    private Rigidbody _carRigidBody;
    private Vector3 _carStartingPosition;
    private Quaternion _carStartingRotation;
    #endregion

    #region Properties
    public GameObject Car {
        get { return _car; }
    }

    public LapManager LapManager {
        get { return _lapMngr; }
    }

    public ReplayCameraManager ReplayCameraManager {
        get {
            return _replayCameraMngr;
        }
    }
    #endregion

    #region Lifecycle
    private void Awake() {
        _autoCam = _cameraRig.GetComponent<UnityStandardAssets.Cameras.AutoCam>();
        _replayCameraCtrl = _cameraRig.GetComponent<ReplayCameraControl>();
        _replayCameraMngr = GetComponent<ReplayCameraManager>();
        _lapMngr = GetComponent<LapManager>();
        _ghostMngr = GetComponent<GhostManager>();
        _lapMngr.GameManager = this;
        _car.GetComponent<CarUserControlPEC1>().GameManager = this;
        _buttonsMngr = GetComponent<ButtonsManager>();
        _carUserCtrl = _car.GetComponent<CarUserControlPEC1>();
        _carCtrller = _car.GetComponent<CarControllerPEC1>();
        _carRigidBody = _car.GetComponent<Rigidbody>();
        _carStartingPosition = _car.transform.position;
        _carStartingRotation = _car.transform.rotation;
    }

    private void Start() {
        _ghostMngr.TrackGhostData = true;
        _replayCameraMngr.TrackData = true;
    }

    private void Update() {
        if(Input.GetButton("Reload")) {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
    }

    public void OnNewLap(int currentLap, int numberOfLaps) {
        if (currentLap < numberOfLaps) {

        }
    }

    public void OnRaceFinished() {
        //Replay();
        _replayCameraMngr.StopTrackingData();
        _ghostMngr.TrackGhostData = false;
        _carUserCtrl.enabled = false;
        _carCtrller.enabled = false;
        _buttonsMngr.SetButtonsActive(true);
        _car.transform.position = _carStartingPosition;
        _car.transform.rotation = _carStartingRotation;
        _carRigidBody.velocity = Vector3.zero;
        _carRigidBody.isKinematic = true;
    }
    #endregion

    #region Methods
    public void Replay() {
        GhostControl ghostCtrl = InstantiateGhost();
        _buttonsMngr.SetButtonsActive(false);
        _replayCameraMngr.ActivateCameraControl();
        _car.SetActive(false);
        _autoCam.enabled = false;
        _replayCameraCtrl.UpdateCamera = true;
    }

    public void Ghost() {
        GhostControl ghostCtrl = InstantiateGhost();
        _buttonsMngr.SetButtonsActive(false);
        _carUserCtrl.enabled = true;
        _carCtrller.enabled = true;
        _carRigidBody.isKinematic = false;
    }

    private GhostControl InstantiateGhost() {
        return _ghostMngr.InstantiateGhost();
    }
    #endregion
}
