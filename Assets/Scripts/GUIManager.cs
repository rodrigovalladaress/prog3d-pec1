﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GUIManager : MonoBehaviour {

    [SerializeField]
    private Text _lapNumberText;
    [SerializeField]
    private Text[] _lapTimesText;
    [SerializeField]
    private string _lapNumberPrefix = "Vuelta ";
    [SerializeField]
    private string _lapTimeBetween = ": ";
    [SerializeField]
    private string _lapTimeSuffix = " s";
    [SerializeField]
    private string _lapTimeFormat = "##0.0";

    public void UpdateLapNumber(int number) {
        _lapNumberText.text = _lapNumberPrefix + (number + 1);
    }

    public void UpdateLapTime(int i, float seconds) {
        if (i < _lapTimesText.Length) {
            _lapTimesText[i].text = (i + 1) + _lapTimeBetween + seconds.ToString(_lapTimeFormat)
            + _lapTimeSuffix;
        }
        else {
            Debug.LogError("Trying to update time of lap " + i + ", but there isn't GUI for that " 
                + "lap");
        }
    }

}
