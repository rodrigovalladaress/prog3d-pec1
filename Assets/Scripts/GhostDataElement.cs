﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Rotación y posición de una parte del coche en un momento dado.
/// </summary>
public class GhostDataElement {

    Vector3 _position;
    Quaternion _rotation;

    public GhostDataElement(Vector3 position, Quaternion rotation) {
        _position = position;
        _rotation = rotation;
    }

    public Vector3 Position {
        get {
            return _position;
        }
    }

    public Quaternion Rotation {
        get {
            return _rotation;
        }
    }

}
