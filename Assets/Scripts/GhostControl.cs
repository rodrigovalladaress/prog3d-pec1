﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GhostControl : MonoBehaviour {

    #region Initialize in inspector
    [SerializeField]
    private Transform _renderRoot;      // Raíz de los renderer del fantasma. Se usa como target de
                                        // la cámara.
    [SerializeField]
    private Transform[] _ghostParts;    // Partes del fantasma que van a actualizarse.
    [SerializeField]
    private Transform _camera;
    [SerializeField]
    private bool _updateCamera;
    #endregion

    #region Private variables
    private GhostData _ghostData;       // Datos del fantasma. Se inicializa desde GhostManager.
    private bool _updateGhost;          // Flag que indica si hay que actualizar la posición/
                                        // rotación del fantasma. Se inicializa desde GhostManager.
    int _t;                             // Iterador de _ghostData (primer nivel, tiempo).     
    float _accumulatedDeltaTime;        // Tiempo transcurrido durante la interpolación actual.
    #endregion

    #region Properties
    public GhostData GhostData {
        get {
            return _ghostData;
        }
        set {
            _ghostData = value;
        }
    }

    public bool UpdateGhost {
        get {
            return _updateGhost;
        }
        set {
            _updateGhost = value;
        }
    }

    public Transform RenderRoot {
        get {
            return _renderRoot;
        }
    }

    public bool UpdateCamera {
        get {
            return _updateCamera;
        }
        set {
            _updateCamera = value;
        }
    }
    #endregion

    #region Lifecycle
    private void Awake() {
        _t = 0;
        _updateGhost = false;
        _accumulatedDeltaTime = 0f;
    }

    // Itera por _ghostData utilizando _t. En cada uno de los puntos de _ghostData, recoge las
    // posiciones y rotaciones guardadas para cada renderer del coche e interpola la posición y 
    // rotación actual de esas partes para que coincida con las guardadas.
    //void Update() { 
    void FixedUpdate() {
        if (_updateGhost && _ghostData != null && _t < _ghostData.TimeCount) {
            float waypointDeltaTime = _ghostData.GetDeltaTime(_t);
            float percentage;  // Porcentaje completado de la interpolación actual de posiciones
                               // y rotaciones.
            
            _accumulatedDeltaTime += Time.fixedDeltaTime;
            //_accumulatedDeltaTime += Time.deltaTime;
            // El primer waypoint es un caso especial, ya que coincide con la posición actual del
            // fantasma, por lo que el porcentaje de completitud debe ser 1.
            if (waypointDeltaTime == 0f) {
                percentage = 1f;
            }
            else {
                percentage = _accumulatedDeltaTime / waypointDeltaTime;
            }
            // Cuando la interpolación del punto actual haya terminado, se avanza al siguiente
            // punto.
            if (percentage >= 1f) {
                _t++;
                //Debug.Log("t = " + _t);
                _accumulatedDeltaTime = 0f;
                percentage = 0f;
            }
            // Se itera por todos los renderers del coche guardados y se procede a interpolar sus 
            // posiciones y rotaciones para que coincidan con las de _ghostData[t][i].
            for (int i = 0; i < _ghostParts.Length; i++) {
                Transform part = _ghostParts[i];
                LerpGhostData(part, _ghostData.GetGhostData(i, _t),
                    _ghostData.GetGhostData(i, _t - 1), percentage);
            }
            if (_updateCamera && _camera) {
                LerpGhostData(_camera, _ghostData.GetCameraData(_t), 
                    _ghostData.GetCameraData(_t - 1), percentage);
            }
        }
    }

    private void LerpGhostData(Transform part, GhostDataElement data, GhostDataElement dataPrev,
        float percentage) {
        if (data != null) {
            if (dataPrev == null) {
                dataPrev = data;
            }
            part.position = Vector3.Slerp(dataPrev.Position, data.Position, percentage);
            part.rotation = Quaternion.Slerp(dataPrev.Rotation, data.Rotation, percentage);
        }
        else {
            Debug.Log("null = " + part.name);
        }
    }

    #endregion

    public void PrintParts() {
        Debug.Log("GHOST CONTROL ------------");
        string print = "{";
        foreach (var item in _ghostParts) {
            print += item.name + ", ";
        }
        print += "}";
        Debug.Log(print);
    }

}
