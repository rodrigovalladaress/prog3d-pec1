﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/// <summary>
/// Recoge los triggers del collider de una parte de una sección.
/// Cuando ocurre una colisión, esta clase  informa a la clase Section, la cual recoge todas las
/// colisiones de todas las partes de una sección.
/// </summary>
public class SectionTrigger : MonoBehaviour {

    #region Private variables
    private Section _sectionRoot;       
    private int _number = -1;           // Se inicializa en Section.
    #endregion

    #region Properties
    public int Number {
        get {
            return _number;
        }
        set {
            _number = value;
        }
    }
    #endregion

    #region Life cycle
    private void Awake() {
        _sectionRoot = GetComponentInParent<Section>();
    }
    #endregion

    #region Triggers
    private void OnTriggerEnter(Collider other) {
        if (other.tag == _sectionRoot.CarTag) {
            if (_sectionRoot.Debug) {
                Debug.Log("Entering section " + _sectionRoot.Number + " (" + _number + ")");
            }
            _sectionRoot.OnChildTriggerEnter(_number);
        }
    }

    private void OnTriggerStay(Collider other) {
        if (other.tag == _sectionRoot.CarTag) {
            if (_sectionRoot.Debug) {
                Debug.Log("Staying in section " + _sectionRoot.Number + " (" + _number + ")");
            }
            _sectionRoot.OnChildTriggerStay(_number);
        }
    }

    private void OnTriggerExit(Collider other) {
        if (other.tag == _sectionRoot.CarTag) {
            if (_sectionRoot.Debug) {
                Debug.Log("Exiting section " + _sectionRoot.Number + " (" + _number + ")");
            }
            _sectionRoot.OnChildTriggerExit(_number);
        }
    }
    #endregion

}
